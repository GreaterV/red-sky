import Vector2                      from "./vector2.js";
import Size2                        from "./size2.js";
import Rectangle2                   from "./rectangle2.js";
import Viewport                     from "./viewport.js";

import { describing, dispatching, casting } from "./meta/meta.js";


const {
    Assert,
    InstanceOf,
    ArrayOf,
    In,
    number,
    string,
} = describing.shortcuts;
const {
    Switch,
    Case,
    Default,
} = dispatching;
const {
    To,
    Cast,
} = casting;


class Camera {
    constructor() {
        this.__position = Vector2.Zero;
        this.__size     = new Size2({
            Width   : 320,
            Height  : 200,
        });
    }

    get Position() {
        return this.__position;
    }
    set Position(value) {
        Assert(value, InstanceOf(Vector2));

        this.__position = value;
    }
    get Size() {
        return this.__size;
    }
    get Area() {
        const position  = this.Position;
        const size      = this.Size;

        return new Rectangle2({
            Left    : position.X - size.X / 2,
            Right   : position.X + size.X / 2,
            Bottom  : position.Y - size.Y / 2,
            Top     : position.Y + size.Y / 2,
        });
    }
}


class Tile {
    static get Size() {
        const size = 16;

        return new Size2({
            Width   : size,
            Height  : size,
        });
    }

    constructor({ Index }) {
        Assert(Index, InstanceOf(Vector2));

        this.__index = Index;
    }

    get Index() {
        return this.__index;
    }
    get Position() {
        return this.Index.Multiply(this.Size);
    }
    get Area() {
        const position  = this.Position;
        const size      = this.Size;

        return new Rectangle2({
            Left    : position.X - size.X / 2,
            Right   : position.X + size.X / 2,
            Bottom  : position.Y - size.Y / 2,
            Top     : position.Y + size.Y / 2,
        });
    }
    get Size() {
        return Tile.Size;
    }
}
class Tiles {
    constructor() {
        this.__tilesX = new Map(); // Map<x index, Map<y index, Tile>>
    }

    __IndexOf(position) {
        Assert(position, InstanceOf(Vector2));

        return position.Divide(Tile.Size).Round;
    }
    __PositionOf(index) {
        Assert(index, InstanceOf(Vector2));

        return index.Multiply(Tile.Size);
    }
    __Generate(index) {
        Assert(index, InstanceOf(Vector2));

        if (index.Y > 0) {
            return null;
        }
        
        const tile      = new Tile({
            Index       : index.Copy,
        });

        if (!this.__tilesX.has(index.X)) {
            this.__tilesX.set(index.X, new Map());
        }
        
        const tilesY    = this.__tilesX.get(index.X);
        
        tilesY.set(index.Y, tile);

        return tile;
    }
    __Find(index) {
        Assert(index, InstanceOf(Vector2));

        if (!this.__tilesX.has(index.X)) {
            return null;
        }

        const tilesY    = this.__tilesX.get(index.X);

        if (!tilesY.has(index.Y)) {
            return null;
        }

        const tile      = tilesY.get(index.Y);

        return tile;
    }
    __FindOrGenerate(index) {
        Assert(index, InstanceOf(Vector2));

        const existed = this.__Find(index);

        if (existed !== null) {
            return existed;
        }

        const generated = this.__Generate(index);

        return generated;
    }

    At(position) {
        Assert(position, InstanceOf(Vector2));

        const index     = this.__IndexOf(position);
        const tile      = this.__FindOrGenerate(index);

        return tile;
    }
    In(area) {
        Assert(area, InstanceOf(Rectangle2));

        const leftBottomIndex   = this.__IndexOf(area.LeftBottom);
        const rightTopIndex     = this.__IndexOf(area.RightTop);

        let tiles               = [];
        let index               = leftBottomIndex.Copy;

        for (; index.X <= rightTopIndex.X; ++index.X) {
            for (index.Y = leftBottomIndex.Y; index.Y <= rightTopIndex.Y; ++index.Y) {
                const tile = this.__FindOrGenerate(index);
    
                if (tile !== null) {
                    tiles.push(tile);
                }
            }
        }

        return tiles;
    }
}


class ScrollState {
    static get Idle() {
        return IdleScrollState;
    }
}
class IdleScrollState extends ScrollState {
    constructor({ Selection = new UnselectedSelectionState() }) {
        Assert(Selection, InstanceOf(SelectionState));

        super();

        this.__selection = Selection;
    }

    get Selection() {
        return this.__selection;
    }
    set Selection(value) {
        this.__selection = value;
    }
}
class MoveScrollState extends ScrollState {
    constructor({ Position }) {
        Assert(Position, InstanceOf(Vector2));

        super();

        this.__position = Position;
    }

    get Position() {
        return this.__position;
    }
}

class SelectionState {
    static get Selected() {
        return SelectedSelectionState;
    }
}
class UnselectedSelectionState extends SelectionState {
}
class SelectedSelectionState extends SelectionState {
    constructor({ Tile }) {
        Assert(Tile, InstanceOf(TileClass));

        super();

        this.__tile = Tile;
    }

    get Tile() {
        return this.__tile;
    }
    set Tile(value) {
        this.__tile = value;
    }
}

class PickState {
    static get Picked() {
        return PickedPickState;
    }
    static get Change() {
        return ChangePickState;
    }
}
class UnpickedPickState extends PickState {
}
class PickedPickState extends PickState {
    constructor({ Tile }) {
        Assert(Tile, InstanceOf(TileClass));

        super();

        this.__tile = Tile;
    }

    get Tile() {
        return this.__tile;
    }
}
class ChangePickState extends PickState {
    constructor({ Next, Previous }) {
        Assert(Next,        InstanceOf(PickState));
        Assert(Previous,    InstanceOf(PickState));

        super();

        this.__next     = Next;
        this.__previous = Previous;
    }

    get [To(PickedPickState)]() {
        return Cast(this.Previous, PickedPickState);
    }

    get Next() {
        return this.__next;
    }
    get Previous() {
        return this.__previous;
    }
}


export default class Game {
    static get Scroll() {
        return ScrollState;
    }
    static get Selection() {
        return SelectionState;
    }
    static get Pick() {
        return PickState;
    }

    constructor(viewport) {
        Assert(viewport, InstanceOf(Viewport));

        const   camera  = new Camera();
        const   tiles   = new Tiles();
        let     scroll  = new IdleScrollState({});
        let     pick    = new UnpickedPickState();

        const   world   = event => event.Position
            .Divide(viewport.Size)
            .Multiply(camera.Size)
            .Subtract(camera.Size.Half)
            .Add(camera.Position)
            ;

        viewport.Listen(event => {
            const position = world(event);

            Switch(scroll, {
                [Case(IdleScrollState)]                     : () => {
                    const tile = tiles.At(position);

                    Switch(scroll.Selection, {
                        [Case(UnselectedSelectionState)]    : () => {
                            if (tile !== null) {
                                scroll.Selection = new SelectedSelectionState({ Tile : tile });
                            }
                        },
                        [Case(SelectedSelectionState)]      : () => {
                            if (tile === null) {
                                scroll.Selection = new UnselectedSelectionState();
                            }
                            else {
                                scroll.Selection.Tile = tile;
                            }
                        },
                    });
                    Switch(event, {
                        [Case(Viewport.Event.Mouse.Down)]   : () => {
                            scroll = new MoveScrollState({ Position : position });

                            pick = tile !== null
                                ? new ChangePickState({ Next : new PickedPickState({ Tile : tile }), Previous : pick })
                                : new ChangePickState({ Next : new UnpickedPickState(), Previous : pick })
                                ;
                        },
                    });
                },
                [Case(MoveScrollState)]                     : () => {
                    const offset    = scroll.Position.Subtract(position);
                    
                    camera.Position = camera.Position.Add(offset);

                    Switch(event, {
                        [Case(Viewport.Event.Mouse.Up)]     : () => {
                            const tile      = tiles.At(position);
                            const selection = tile !== null
                                ? new SelectedSelectionState({ Tile : tile })
                                : new UnselectedSelectionState();

                            scroll          = new IdleScrollState({ Selection : selection });
                        },
                    });
                },
            });
            Switch(pick, {
                [Case(ChangePickState)]                         : () => {
                    Switch(event, {
                        [Case(Viewport.Event.Mouse.Move)]       : () => {
                            pick = pick.Previous;
                        },
                        [Case(Viewport.Event.Mouse.Up)]         : () => {
                            pick = pick.Next;
                        },
                    });
                },
            });
            
            this.__scroll       = scroll;
            this.__pick         = pick;
        });

        this.__camera       = camera;
        this.__tiles        = tiles;
        this.__scroll       = scroll;
        this.__pick         = pick;
    }

    get Camera() {
        return this.__camera;
    }
    get Tiles() {
        return this.__tiles;
    }
    get Scroll() {
        return this.__scroll;
    }
    get Pick() {
        return this.__pick;
    }
}


const TileClass = Tile;
