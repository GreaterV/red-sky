import Viewport     from "./viewport.js";
import Drawer       from "./drawer.js";
import Rectangle2   from "./rectangle2.js";
import Vector2      from "./vector2.js";
import Game         from "./game.js";

import { casting }  from "./meta/meta.js";

const {
    To,
    Cast,
} = casting;

const ID_CANVAS = `id-canvas`;


function get_canvas() {
    const canvas = document.getElementById(ID_CANVAS);

    if (!canvas) {
        throw new Error(`Canvas does not exists.`);
    }

    return canvas;
}


function main() {
    const canvas    = get_canvas();
    const viewport  = new Viewport(canvas);
    const drawer    = new Drawer(viewport);

    const images    = {
        selection   : drawer.LoadTexture(`media/images/selection.png`),
        ground      : [
            drawer.LoadTexture(`media/images/ground_1.png`),
        ],
    };

    const game      = new Game(viewport);

    function rect(input) {
        return input
            .Move(game.Camera.Position.Inverse)
            .Scale(Vector2.One.Divide(game.Camera.Size))
            .Move(Vector2.Half)
            .Scale(viewport.Size)
            ;
    }

    function draw() {
        drawer.Clear();

        const tiles = game.Tiles.In(game.Camera.Area);

        for (const tile of tiles) {
            drawer.Draw(images.ground[0], rect(tile.Area));
        }

        if (game.Scroll instanceof Game.Scroll.Idle && game.Scroll.Selection instanceof Game.Selection.Selected) {
            drawer.Draw(images.selection, rect(game.Scroll.Selection.Tile.Area));
        }

        const picked = Cast(game.Pick, Game.Pick.Picked);

        if (picked !== null) {
            drawer.Draw(images.selection, rect(picked.Tile.Area));
        }

        const change = Cast(game.Pick, Game.Pick.Change);

        if (change !== null) {
            const nextPicked = Cast(change.Next, Game.Pick.Picked);

            if (nextPicked !== null) {
                drawer.Draw(images.selection, rect(nextPicked.Tile.Area));
            }
        }

        requestAnimationFrame(draw);
    }
    
    requestAnimationFrame(draw);
}


window.addEventListener(`load`, main);
