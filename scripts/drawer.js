import Viewport from "./viewport.js";
import Vector2 from "./vector2.js";

import { describing } from "./meta/meta.js";
import Rectangle2 from "./rectangle2.js";


const {
    Assert,
    InstanceOf,
    ArrayOf,
    In,
    number,
    string,
} = describing.shortcuts;


export default class Drawer {
    constructor(viewport) {
        Assert(viewport, InstanceOf(Viewport));


        const canvas    = viewport.__Canvas;
        const gl        = canvas.getContext(`webgl`);
        
        if (gl === null) {
            throw new Error(`Failed to get WebGL render context.`);
        }
        

        const vertexShader              = gl.createShader(gl.VERTEX_SHADER);
        const vertexShaderSource        = `
            attribute   vec2        positionAttribute;
            attribute   vec2        textureCoordinatesAttribute;

            varying     highp vec2  vertexTextureCoordinates;
    
            void main() {
                gl_Position = vec4(positionAttribute, 0, 1);

                vertexTextureCoordinates = textureCoordinatesAttribute;
            }
        `;
    
        gl.shaderSource(vertexShader, vertexShaderSource);
        gl.compileShader(vertexShader);
        
        if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
            const log = gl.getShaderInfoLog(vertexShader);
    
            throw new Error(`Failed to compile vertex shader: ${log}`);
        }
    
        const fragmentShader            = gl.createShader(gl.FRAGMENT_SHADER);
        const fragmentShaderSource      = `
            uniform sampler2D   colorTexture;
            varying highp vec2  vertexTextureCoordinates;

            void main() {
                gl_FragColor = texture2D(colorTexture, vertexTextureCoordinates);
            }
        `;
    
        gl.shaderSource(fragmentShader, fragmentShaderSource);
        gl.compileShader(fragmentShader);
        
        if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
            const log = gl.getShaderInfoLog(fragmentShader);
    
            throw new Error(`Failed to compile fragment shader: ${log}`);
        }
    
        const program = gl.createProgram();
    
        gl.attachShader(program, vertexShader);
        gl.attachShader(program, fragmentShader);
        gl.linkProgram(program);
        
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            const log = gl.getProgramInfoLog(program);
    
            throw new Error(`Failed to link program: ${log}`);
        }
    
        gl.deleteShader(vertexShader);
        gl.deleteShader(fragmentShader);
    
        const colorTextureUniformLocation           = gl.getUniformLocation(program, `colorTexture`);

        gl.useProgram(program);

        if (colorTextureUniformLocation !== -1) {
            gl.uniform1i(colorTextureUniformLocation, 0);
        }

        const positionAttributeLocation             = gl.getAttribLocation(program, `positionAttribute`);
        const textureCoordinatesAttributeLocation   = gl.getAttribLocation(program, `textureCoordinatesAttribute`);

        const vertexBuffer              = gl.createBuffer();
        const vertexBufferSource        = new Float32Array([
            -0.5,   -0.5,       0.0,    0.0,
            +0.5,   -0.5,       1.0,    0.0,
            -0.5,   +0.5,       0.0,    1.0,
            +0.5,   +0.5,       1.0,    1.0,
        ]);
    
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, vertexBufferSource, gl.STATIC_DRAW);

        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

        this.__viewport                             = viewport;
        this.__gl                                   = gl;
        this.__program                              = program;
        this.__positionAttributeLocation            = positionAttributeLocation;
        this.__textureCoordinatesAttributeLocation  = textureCoordinatesAttributeLocation;
        this.__vertexBuffer                         = vertexBuffer;
    }

    LoadTexture(url) {
        Assert(url, string);

        const gl        = this.__gl;
        const texture   = gl.createTexture();

        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([ 0, 0, 255, 255 ]));
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

        const image = new Image();

        image.onload = function() {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        };

        image.src = url;

        return texture; // @todo: replace with class
    }
    Draw(texture, rectangle) {
        Assert(rectangle, InstanceOf(Rectangle2));

        const gl            = this.__gl;
        const size          = this.__viewport.Size;

        const leftBottom    = rectangle.LeftBottom.Round.Divide(size).Multiply(Vector2.Two).Subtract(Vector2.One);
        const rightTop      = rectangle.RightTop.Round.Divide(size).Multiply(Vector2.Two).Subtract(Vector2.One);

        const vertexBufferSource        = new Float32Array([
            leftBottom.X,   leftBottom.Y,   0.0,    0.0,
            rightTop.X,     leftBottom.Y,   1.0,    0.0,
            leftBottom.X,   rightTop.Y,     0.0,    1.0,
            rightTop.X,     rightTop.Y,     1.0,    1.0,
        ]);
    
        gl.bindBuffer(gl.ARRAY_BUFFER, this.__vertexBuffer);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, vertexBufferSource);

        if (this.__positionAttributeLocation !== -1) {
            gl.vertexAttribPointer(this.__positionAttributeLocation, 2, gl.FLOAT, false, 4*Float32Array.BYTES_PER_ELEMENT, 0);
            gl.enableVertexAttribArray(this.__positionAttributeLocation);
        }
        if (this.__textureCoordinatesAttributeLocation !== -1) {
            gl.vertexAttribPointer(this.__textureCoordinatesAttributeLocation, 2, gl.FLOAT, false, 4*Float32Array.BYTES_PER_ELEMENT, 2*Float32Array.BYTES_PER_ELEMENT);
            gl.enableVertexAttribArray(this.__textureCoordinatesAttributeLocation);
        }

        gl.useProgram(this.__program);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture);

        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
    }
    Clear() {
        const gl = this.__gl;

        gl.clearColor(1, 0.5, 0, 1);
        gl.clear(gl.COLOR_BUFFER_BIT);
    }
}
