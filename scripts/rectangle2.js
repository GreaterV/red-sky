import Vector2  from "./vector2.js";
import Size2    from "./size2.js";

import { describing } from "./meta/meta.js";


const {
    Assert,
    InstanceOf,
    ArrayOf,
    In,
    number,
    string,
} = describing.shortcuts;


export default class Rectangle2 {
    constructor({ Left, Right, Top, Bottom }) {
        // @todo: assert

        this.__left     = Left;
        this.__right    = Right;
        this.__bottom   = Bottom;
        this.__top      = Top;
    }

    get Left() {
        return this.__left;
    }
    get Right() {
        return this.__right;
    }
    get Bottom() {
        return this.__bottom;
    }
    get Top() {
        return this.__top;
    }
    get Size() {
        const width     = this.Right - this.Left;
        const height    = this.Top - this.Bottom;
        const size      = new Size2({
            Width   : width,
            Height  : height,
        });
        
        return size;
    }
    get LeftBottom() {
        return new Vector2({
            X   : this.Left,
            Y   : this.Bottom,
        });
    }
    get RightTop() {
        return new Vector2({
            X   : this.Right,
            Y   : this.Top,
        });
    }

    Move(offset) {
        Assert(offset, InstanceOf(Vector2));

        return new Rectangle2({
            Left    : this.Left + offset.X,
            Right   : this.Right + offset.X,
            Bottom  : this.Bottom + offset.Y,
            Top     : this.Top + offset.Y,
        });
    }
    Scale(scale) {
        Assert(scale, InstanceOf(Vector2));

        return new Rectangle2({
            Left    : this.Left * scale.X,
            Right   : this.Right * scale.X,
            Bottom  : this.Bottom * scale.Y,
            Top     : this.Top * scale.Y,
        });
    }
}
