import { describing } from "./meta/meta.js";


const {
    Assert,
    InstanceOf,
    ArrayOf,
    In,
    number,
    string,
} = describing.shortcuts;


export default class Vector2 {
    static get Zero() {
        return new Vector2({});
    }
    static get Half() {
        return new Vector2({ X : 0.5, Y : 0.5 });
    }
    static get One() {
        return new Vector2({ X : 1, Y : 1 });
    }
    static get Two() {
        return new Vector2({ X : 2, Y : 2 });
    }
    
    static Single(value) {
        return new Vector2({
            X   : value,
            Y   : value,
        });
    }

    constructor({ X = 0, Y = 0 }) {
        Assert(X,   number);
        Assert(Y,   number);
        
        this.__x    = X;
        this.__y    = Y;
    }

    get X() {
        return this.__x;
    }
    set X(value) {
        Assert(value, number);

        this.__x = value;
    }
    get Y() {
        return this.__y;
    }
    set Y(value) {
        Assert(value, number);

        this.__y = value;
    }

    get Inverse() {
        return new Vector2({
            X   : -this.X,
            Y   : -this.Y,
        });
    }
    get Half() {
        return new Vector2({
            X   : this.X / 2,
            Y   : this.Y / 2,
        });
    }
    get Round() {
        return new Vector2({
            X   : Math.round(this.X),
            Y   : Math.round(this.Y),
        });
    }
    get Copy() {
        return new Vector2(this);
    }

    Add(other) {
        Assert(other, InstanceOf(Vector2));
        
        return new Vector2({
            X   : this.X + other.X,
            Y   : this.Y + other.Y,
        });
    }
    Subtract(other) {
        Assert(other, InstanceOf(Vector2));
        
        return new Vector2({
            X   : this.X - other.X,
            Y   : this.Y - other.Y,
        });
    }
    Multiply(other) {
        Assert(other, InstanceOf(Vector2));
        
        return new Vector2({
            X   : this.X * other.X,
            Y   : this.Y * other.Y,
        });
    }
    Divide(other) {
        Assert(other, InstanceOf(Vector2));
        
        return new Vector2({
            X   : this.X / other.X,
            Y   : this.Y / other.Y,
        });
    }
}
