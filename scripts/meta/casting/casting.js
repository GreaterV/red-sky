"use strict";


const getCastKeyKey = Symbol(`Symbol for obtaining class-specific cast key`);


function getCastKey(Type) {
    const castKey = Type[getCastKeyKey](Type);

    return castKey;
}
function generateCastKey(Type) {
    if (Type === Object) {
        throw new Error(); // @todo
    }

    const castKey = Symbol(`Symbol for obtaining cast function for class ${JSON.stringify(Type.name)}`);

    function getCastKey() {
        return castKey;
    }

    Object.defineProperty(Type, getCastKeyKey, {
        configurable    : false,
        enumerable      : false,
        writable        : false,
        value           : getCastKey,
    });
    
    function cast() {
        return null;
    }

    Object.defineProperty(Object.prototype, castKey, {
        configurable    : false,
        enumerable      : false,
        get             : cast,
    });

    function selfCast() {
        return this;
    }

    Object.defineProperty(Type.prototype, castKey, {
        configurable    : false,
        enumerable      : false,
        get             : selfCast,
    });

    return castKey;
}


Object.defineProperty(Object.prototype, getCastKeyKey, {
    configurable    : false,
    enumerable      : false,
    writable        : false,
    value           : generateCastKey,
});


function To(Type) {
    const castKey = getCastKey(Type);

    return castKey;
}
function Cast(instance, Type) {
    const castKey   = getCastKey(Type);
    const casted    = instance[castKey];

    return casted;
}


export { To, Cast };
