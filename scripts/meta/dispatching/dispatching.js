"use strict";


import { Dispatcher }               from "./dispatcher.js";
import { Switch, Case, Default }    from "./switcher.js";


export { Dispatcher, Switch, Case, Default };
