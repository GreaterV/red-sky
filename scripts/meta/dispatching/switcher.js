"use strict";


const getKeyKey         = Symbol(); // @todo
const getTypeKeyKey     = Symbol(); // @todo
const defaultKey        = Symbol(); // @todo


function getObjectKey(Type) {
    const key = Symbol(`Switcher key for the ${JSON.stringify(Type.name)} class.`);
    
    function getKey(OtherType) {
        return OtherType === Type
            ? key
            : getObjectKey(OtherType);
    }
    function getTypeKey() {
        return key;
    }
    function defaultHandler() {
        return this[defaultKey]();
    }
        
    Object.defineProperty(Type, getKeyKey, {
        configurable    : false,
        enumerable      : false,
        writable        : false,
        value           : getKey,
    });
    Object.defineProperty(Type.prototype, getTypeKeyKey, {
        configurable    : false,
        enumerable      : false,
        writable        : false,
        value           : getTypeKey,
    });
    Object.defineProperty(Object.prototype, key, {
        configurable    : false,
        enumerable      : false,
        writable        : false,
        value           : defaultHandler,
    });

    return key;
}
function getObjectTypeKey() {
    return defaultKey;
}
function defaultHandler() {
    // do nothing
}

Object.defineProperty(Object.prototype, getKeyKey, {
    configurable    : false,
    enumerable      : false,
    writable        : false,
    value           : getObjectKey,
});
Object.defineProperty(Object.prototype, getTypeKeyKey, {
    configurable    : false,
    enumerable      : false,
    writable        : false,
    value           : getObjectTypeKey,
});
Object.defineProperty(Object.prototype, defaultKey, {
    configurable    : false,
    enumerable      : false,
    writable        : false,
    value           : defaultHandler,
});


function Switch(instance, handlers) {
    const typeKey   = instance[getTypeKeyKey]();
    const result    = handlers[typeKey](instance);

    return result;
}
function Case(Type) {
    const typeKey = Type[getKeyKey](Type);

    return typeKey;
}
function Default() {
    return defaultKey;
}


/*class A {}
class B {}
class C extends B {}
class D extends B {}
class E extends D {}

const a = new A();
const b = new B();
const c = new C();
const d = new D();
const e = new E();


console.log(`A: ${Switch(a, {
    [Case(A)]   : () => "A",
})}`);
console.log(`A: ${Switch(a, {
    [Case(A)]   : () => "A",
    [Case(B)]   : () => "B",
})}`);
console.log(`B: ${Switch(b, {
    [Case(A)]   : () => "A",
    [Case(B)]   : () => "B",
})}`);
console.log(`B: ${Switch(b, {
    [Case(B)]   : () => "B",
    [Case(C)]   : () => "C",
})}`);
console.log(`C: ${Switch(c, {
    [Case(B)]   : () => "B",
    [Case(C)]   : () => "C",
})}`);
console.log(`undefined: ${Switch(b, {
    [Case(A)]   : () => "undefined",
})}`);
console.log(`default: ${Switch(b, {
    [Case(A)]   : () => "A",
    [Default()] : () => "default",
})}`);*/


export { Switch, Case, Default };
