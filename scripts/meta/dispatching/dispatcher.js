"use strict";


import { Meta } from "./../header.js";


// dispatcher
function Dispatcher(...Types) {
    const redirectKey               = Symbol();
    const dispatchKeyKey            = Symbol();
    const dispatchUnspecifiedKey    = Symbol();
    const dispatchOtherKey          = Symbol();

    for (const Type of Types) {
        const dispatchKey = Symbol();

        Object.defineProperty(Type, dispatchKeyKey, {
            configurable    : false,
            enumerable      : false,
            writable        : false,
            value           : dispatchKey,
        });
    }

    function Dispatch(Type) {
        if (dispatchKeyKey in Type); else {
            throw new Error();
        }

        const dispatchKey = Type[dispatchKeyKey];

        return dispatchKey;
    }

    Dispatch(Types[0]);

    Object.defineProperty(Dispatch, "Unspecified", {
        configurable    : false,
        enumerable      : true,
        writable        : false,
        value           : dispatchUnspecifiedKey,
    });
    Object.defineProperty(Dispatch, "Other", {
        configurable    : false,
        enumerable      : true,
        writable        : false,
        value           : dispatchOtherKey,
    });

    class Dispatcher extends Meta {
        static get Dispatch() {
            return Dispatch;
        }

        [dispatchUnspecifiedKey](instance, ...other) {
            return this[dispatchOtherKey](instance, ...other);
        }
        [dispatchOtherKey](instance, ...other) {
            // do nothing
        }

        Dispatch(instance, ...other) {
            return instance[redirectKey](this, ...other);
        }
    }

    for (const Type of Types) {
        const dispatchKey = Type[dispatchKeyKey];

        function dispatch(instance, ...other) {
            return this[dispatchOtherKey](instance, ...other);
        }

        Object.defineProperty(Dispatcher.prototype, dispatchKey, {
            configurable    : false,
            enumerable      : false,
            writable        : false,
            value           : dispatch,
        });

        function redirect(dispatcher, ...other) {
            return dispatcher[dispatchKey](this, ...other);
        }

        Object.defineProperty(Type.prototype, redirectKey, {
            configurable    : false,
            enumerable      : false,
            writable        : false,
            value           : redirect,
        });
    }

    return Dispatcher;
}


export { Dispatcher };
