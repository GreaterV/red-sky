"use strict";


import { Meta } from "./../header.js";
import { Dispatcher } from "./../dispatching/dispatching.js";


class Descriptor extends Meta {
    static get Type() {
        return Type;
    }
    static get ArrayOf() {
        return ArrayOf;
    }
}
class Or extends Descriptor {
    constructor({ Descriptors }) {
        if (Descriptors instanceof Array && Descriptors.every(descriptor => descriptor instanceof Descriptor)); else {
            throw new Error();
        }

        super();

        this.Descriptors = Descriptors;
    }
}
class Type extends Descriptor {
    static get Primitive() {
        return PrimitiveType;
    }
    static get object() {
        return objectType;
    }
}
class PrimitiveType extends Type {
    static get Boolean() {
        return BooleanPrimitiveType;
    }
    static get Number() {
        return NumberPrimitiveType;
    }
    static get String() {
        return StringPrimitiveType;
    }
}
class BooleanPrimitiveType extends PrimitiveType {
}
class NumberPrimitiveType extends PrimitiveType {
}
class StringPrimitiveType extends PrimitiveType {
}
class ObjectType extends Type {
    constructor({ Class }) {
        if (Class instanceof Function); else {
            throw new Error(); // @todo
        }

        super();

        this.Class = Class;
    }
}
class Value extends Descriptor {
}
class Null extends Value {
}
class ArrayOfDescriptor extends Descriptor {
    constructor({ Element }) {
        if (Element instanceof Descriptor); else {
            throw new Error(); // @todo
        }

        super();

        this.Element = Element;
    }
}
class InDescriptor extends Descriptor {
    constructor({ Collection }) {
        if (Collection instanceof Object); else {
            throw new Error(); // @todo
        }

        super();

        this.Collection = Collection;
    }
}


const DeducingDispatcher = Dispatcher(
    Descriptor,
);

class Deducer extends DeducingDispatcher {
    [DeducingDispatcher.Dispatch.Other](description) {
        throw new Error(`Can't deduce descriptor from description: ${description}.`);
    }
    [DeducingDispatcher.Dispatch(Descriptor)](descriptor) {
        return descriptor;
    }

    Deduce(description) {
        if (description === null) {
            return new Null();
        }

        const descriptor = this.Dispatch(description);

        return descriptor;
    }
}

const deducer = new Deducer();

function Deduce(...descriptions) {
    return descriptions.length > 1
        ? new Or({ Descriptors : descriptions.map(description => deducer.Deduce(description)) })
        : deducer.Deduce(descriptions[0]);
}


const AssertingDispatcher = Dispatcher(
    Or,
    BooleanPrimitiveType,
    NumberPrimitiveType,
    StringPrimitiveType,
    ObjectType,
    Null,
    ArrayOfDescriptor,
    InDescriptor,
);

class Asserter extends AssertingDispatcher {
    [AssertingDispatcher.Dispatch.Other](descriptor, object) {
        throw new Error(); // @todo
    }
    [AssertingDispatcher.Dispatch(Or)](or, object) {
        return or.Descriptors.some(descriptor => this.Dispatch(descriptor, object));
    }
    [AssertingDispatcher.Dispatch(BooleanPrimitiveType)](boolean, object) {
        return typeof object === "boolean";
    }
    [AssertingDispatcher.Dispatch(NumberPrimitiveType)](number, object) {
        return typeof object === "number";
    }
    [AssertingDispatcher.Dispatch(StringPrimitiveType)](string, object) {
        return typeof object === "string";
    }
    [AssertingDispatcher.Dispatch(ObjectType)](instanceOf, object) {
        return object instanceof instanceOf.Class;
    }
    [AssertingDispatcher.Dispatch(Null)](null_, object) {
        return object === null;
    }
    [AssertingDispatcher.Dispatch(ArrayOfDescriptor)](arrayOf, object) {
        if (object instanceof Array); else {
            return false;
        }
        
        return object.every(element => this.Dispatch(arrayOf.Element, element));
    }
    [AssertingDispatcher.Dispatch(InDescriptor)](inDescriptor, object) {
        const values = Object.values(inDescriptor.Collection);

        return values.indexOf(object) !== -1;
    }

    Assert(object, descriptor) {
        const result = this.Dispatch(descriptor, object);

        if (!result) {
            throw new Error(); // @todo
        }
    }
}

const asserter = new Asserter();

function Assert(object, ...descriptions) {
    const descriptor = Deduce(...descriptions);

    asserter.Assert(object, descriptor);
}


const boolean   = new Descriptor.Type.Primitive.Boolean();
const number    = new Descriptor.Type.Primitive.Number();
const string    = new Descriptor.Type.Primitive.String();


function InstanceOf(Class) {
    const descriptor = new ObjectType({ Class });

    return descriptor;
}
function ArrayOf(description) {
    const Element       = Deduce(description);
    const descriptor    = new ArrayOfDescriptor({ Element });

    return descriptor;
}
function In(Collection) {
    return new InDescriptor({ Collection });
}


const shortcuts = {
    boolean,
    number,
    string,

    Deduce,
    Assert,
    InstanceOf,
    ArrayOf,
    In
};


export { Descriptor, shortcuts };
