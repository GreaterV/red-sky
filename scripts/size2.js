import Vector2 from "./vector2.js";

import { describing } from "./meta/meta.js";


const {
    Assert,
    InstanceOf,
    ArrayOf,
    In,
    number,
    string,
} = describing.shortcuts;


export default class Size2 extends Vector2 {
    constructor({ Width, Height }) {
        Assert(Width,   number);
        Assert(Height,  number);

        super({ X : Width, Y : Height });
    }
    get Width() {
        return this.X;
    }
    set Width(value) {
        Assert(value, number);

        this.X = value;
    }
    get Height() {
        return this.Y;
    }
    set Height(value) {
        Assert(value, number);
        
        this.Y = value;
    }
}
