import Rectangle2   from "./rectangle2.js";
import Vector2      from "./vector2.js";

import { describing } from "./meta/meta.js";


const {
    Assert,
    InstanceOf,
    ArrayOf,
    In,
    number,
    string,
} = describing.shortcuts;


class Event {
    static get Mouse() {
        return MouseEvent;
    }
}
class MouseEvent extends Event {
    static get Down() {
        return DownMouseEvent;
    }
    static get Up() {
        return UpMouseEvent;
    }
    static get Move() {
        return MoveMouseEvent;
    }

    constructor({ Position }) {
        Assert(Position, InstanceOf(Vector2));

        super();

        this.__position = Position;
    }

    get Position() {
        return this.__position;
    }
}
class DownMouseEvent extends MouseEvent {
}
class UpMouseEvent extends MouseEvent {
}
class MoveMouseEvent extends MouseEvent {
}


class Listener {
    constructor({ Viewport, Callback }) {
        Assert(Viewport, InstanceOf(ViewportClass));

        function get_mouse_position(event) {
            const x         = event.pageX - Viewport.Left;
            const y         = Viewport.Height - (event.pageY - Viewport.Top);
            const position  = new Vector2({
                X   : x,
                Y   : y,
            });

            return position;
        }
        function on_mouse_down(event) {
            const position  = get_mouse_position(event);
            const down      = new DownMouseEvent({ Position : position });

            Callback(down);
        }
        function on_mouse_up(event) {
            const position  = get_mouse_position(event);
            const up        = new UpMouseEvent({ Position : position });

            Callback(up);
        }
        function on_mouse_move(event) {
            const position  = get_mouse_position(event);
            const move      = new MoveMouseEvent({ Position : position });

            Callback(move);
        }

        const canvas = Viewport.__Canvas;

        canvas.addEventListener(`mousedown`, on_mouse_down);        
        canvas.addEventListener(`mouseup`,   on_mouse_up);        
        canvas.addEventListener(`mousemove`, on_mouse_move);        

        this.__viewport         = Viewport;
        this.__on_mouse_down    = on_mouse_down;
        this.__on_mouse_up      = on_mouse_up;
        this.__on_mouse_move    = on_mouse_move;
    }

    Stop() {
        if (this.__viewport === null) {
            throw new Error(`Listening was already stopped.`);
        }

        const canvas = this.__viewport.__Canvas;

        canvas.removeEventListener(`mousedown`, this.__on_mouse_down);        
        canvas.removeEventListener(`mouseup`,   this.__on_mouse_up);        
        canvas.removeEventListener(`mousemove`, this.__on_mouse_move);        

        this.__viewport = null;
    }
}


export default class Viewport {
    static get Event() {
        return Event;
    }

    constructor(canvas) {
        // @todo: assert canvas instanceof something

        this.__canvas = canvas;
    }

    get __Canvas() {
        return this.__canvas;
    }

    get Rectangle() {
        const { left, right, top, bottom }  = this.__Canvas.getBoundingClientRect();
        const rectangle                     = new Rectangle2({
            Left    : left,
            Right   : right,
            Top     : top,
            Bottom  : bottom,
        });

        return rectangle;
    }
    get Size() {
        const size = this.Rectangle.Size;
        
        size.Y = -size.Y;
        
        return size;
    }
    get Width() {
        return this.Size.Width;
    }
    get Height() {
        return this.Size.Height;
    }
    get Left() {
        return this.Rectangle.Left;
    }
    get Top() {
        return this.Rectangle.Top;
    }

    Listen(callback) {
        Assert(callback, InstanceOf(Function));
        
        const listener = new Listener({ Viewport : this, Callback : callback });

        return listener;
    }
}


const ViewportClass = Viewport;
